# TimGangReasoner神经符号本体推理机

#### 介绍
面向开放世界假设OWL+SWRL表示本体的ABox推理机，基于OWL-API+Pellet+DL4J+Prefuse等Java开源API实现，可利用GPU进行高效并行计算，推理速度比经典本体推理机如Pellet、Hermit等快数十倍。以下是在简单事件本体和时间本体上，不同规模的ABox（数据，在'sem不同规模'分支中）条件下，与Pellet推理机耗时（受计算机内存限制，对后面规模较大的ABox，Pellet无法完成推理）的对比。
![输入图片说明](https://images.gitee.com/uploads/images/2021/0126/142833_7aea4e61_8048120.jpeg "NSR推理实验.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0126/142847_ef34c3a2_8048120.jpeg "在时间本体上推理的耗时比较.jpg")
#### 软件架构
软件架构说明
![输入图片说明](https://images.gitee.com/uploads/images/2021/0108/092605_8e280f66_8048120.jpeg "绘图1.jpg")
<center>神经符号本体推理机的实现框架</center>


#### 安装教程

1.  安装JDK14
2.  下载TimGangReasoner.bat和TimGangReasoner.jar，并下载引用jar文件列表中的jar，用winRAR打开TimGangReasoner.jar，并将下载的引用jar文件拖入到TimGangReasoner.jar（根目录）中（引用列表在 引用jar文件列表和TGR论文实验数据.txt文件中），上述文件（夹）放到相同目录
3.  安装Nvidia-Driver440.89、Cuda10.2和Cudnn
4.  在Windows操作系统中，双击TimGangReasoner.bat启动软件;在Ubuntu系统中，Terminal运行java -Xms13G -Xmx52G -jar TimGangReasoner.jar启动软件

#### 使用说明

1.  在Windows操作系统中，双击TimGangReasoner.bat启动软件;在Ubuntu系统中，Terminal运行java -Xms13G -Xmx52G -jar TimGangReasoner.jar运行软件
2.  按照帮助中的操作获得配置文件
3.  点击配置菜单栏，打开配置文件即可运行
![输入图片说明](https://images.gitee.com/uploads/images/2021/0114/171002_d61e9ccb_8048120.jpeg "新建位图图像.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0125/154158_fb81d667_8048120.png "2021-01-25 15-40-31屏幕截图.png")
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 参考文献

1.  刘斌, 陈航, 陆敏, 朱席席, 姚莉, & 丁兆云等. 一种图数据合成方法、装置、计算机设备和存储介质[P]. ZL202011480983.
